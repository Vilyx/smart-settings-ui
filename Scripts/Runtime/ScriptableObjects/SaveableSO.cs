﻿using System.IO;
using UnityEngine;

namespace SmartSettingsUI
{
	public class SaveableSO : ScriptableObject, ISaveable
	{
		private static string settingsFileExtension = "json";

		private void OnEnable()
		{
			Load();
		}

		public void Save(string filename = "")
		{
			if (filename.Length == 0)
			{
				filename = name;
			}
			string json = JsonUtility.ToJson(this);
			string fullPath = GetFullPath(filename + "." + settingsFileExtension);

			File.WriteAllText(fullPath, json);
		}

		public void Load(string filename = "")
		{
			if (filename.Length == 0)
			{
				filename = name;
			}
			string fullPath = GetFullPath(filename + "." + settingsFileExtension);

			string json = File.ReadAllText(fullPath);
			JsonUtility.FromJsonOverwrite(json, this);
		}

		private string GetFullPath(string filename = "")
		{
			return Path.Combine(Application.persistentDataPath, filename);
		}
	}
}