﻿using UnityEngine;

namespace SmartSettingsUI
{
	public class SaveableButtonsController : MonoBehaviour
	{

		private ISaveable saveable;

		public void Initialize(ISaveable targetObject)
		{
			saveable = targetObject;
		}

		public void Save()
		{
			saveable.Save();
		}
	} 
}