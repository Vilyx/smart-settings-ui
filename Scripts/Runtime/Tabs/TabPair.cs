﻿using UnityEngine;

namespace SmartSettingsUI
{
	[System.Serializable]
	public class TabPair
	{
		public TabButton tabButton;
		public GameObject objectToSwap;

		public TabPair(TabButton tabButton, GameObject objectToSwap)
		{
			this.tabButton = tabButton;
			this.objectToSwap = objectToSwap;
		}
	};
}