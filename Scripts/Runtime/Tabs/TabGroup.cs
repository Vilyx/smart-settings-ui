﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace SmartSettingsUI
{
	public partial class TabGroup : MonoBehaviour
	{
		public List<TabPair> tabs = new List<TabPair>();
		private TabButton selectedTab;

		private void Start()
		{
			for (int i = 0; i < tabs.Count; i++)
			{
				tabs[i].tabButton.tabGroup = this;
			}
		}

		public void AddTab(TabPair tabPair)
		{
			tabPair.tabButton.tabGroup = this;
			tabs.Add(tabPair);
		}

		public void OnTabEnter(TabButton tabButton)
		{
			ResetTabs();
			if (selectedTab != tabButton)
			{
				tabButton.SetButtonHovered();
			}
		}

		public void OnTabExit(TabButton tabButton)
		{
			ResetTabs();
			if (selectedTab != tabButton)
			{
				tabButton.SetButtonIdle();
			}
		}

		public void OnTabSelected(TabButton tabButton)
		{
			selectedTab = tabButton;
			ResetTabs();
			tabButton.SetButtonSelected();
			int selectedIndex = tabs.FindIndex(tab => tab.tabButton == tabButton);
			for (int i = 0; i < tabs.Count; i++)
			{
				tabs[i].objectToSwap.SetActive(i == selectedIndex);
			}
		}

		private void ResetTabs()
		{
			for (int i = 0; i < tabs.Count; i++)
			{
				if (tabs[i].tabButton == selectedTab)
				{
					continue;
				}
				tabs[i].tabButton.SetButtonIdle();
			}
		}
	}
}