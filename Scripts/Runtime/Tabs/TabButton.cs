﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace SmartSettingsUI
{
	[RequireComponent(typeof(ImageConversion))]
	public class TabButton : MonoBehaviour, IPointerEnterHandler, IPointerClickHandler, IPointerExitHandler
	{
		[HideInInspector]
		public TabGroup tabGroup;
		[SerializeField]
		private Sprite tabIdleSprite;
		[SerializeField]
		private Sprite tabHoverSprite;
		[SerializeField]
		private Sprite tabSelectedSprite;
		[SerializeField]
		private Color tabIdleFontColor;
		[SerializeField]
		private Color tabHoverFontColor;
		[SerializeField]
		private Color tabSelectedFontColor;

		private Image background;
		private Text label;

		private void Awake()
		{
			background = GetComponent<Image>();
			label = GetComponentInChildren<Text>();
		}

		public void OnPointerClick(PointerEventData eventData)
		{
			tabGroup.OnTabSelected(this);
		}

		public void OnPointerEnter(PointerEventData eventData)
		{
			tabGroup.OnTabEnter(this);
		}

		public void OnPointerExit(PointerEventData eventData)
		{
			tabGroup.OnTabExit(this);
		}

		public void SetButtonText(string text)
		{
			label.text = text;
		}

		public void SetButtonHovered()
		{
			background.sprite = tabHoverSprite;
			SetLabelFontColor(tabHoverFontColor);
		}

		public void SetButtonIdle()
		{
			background.sprite = tabIdleSprite;
			SetLabelFontColor(tabIdleFontColor);
		}

		public void SetButtonSelected()
		{
			background.sprite = tabSelectedSprite;
			SetLabelFontColor(tabSelectedFontColor);
		}

		private void SetLabelFontColor(Color newColor)
		{
			if (label != null)
			{
				label.color = newColor;
			}
		}
	}
}