﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace SmartSettingsUI
{
	public class SettingsWindow : MonoBehaviour
	{
		[SerializeField]
		private List<ScriptableObject> setttingsObjects;
		[SerializeField]
		private TabGroup tabGroup;
		[SerializeField]
		private Transform tabButtonsHolder;
		[SerializeField]
		private Transform pagesHolder;
		[SerializeField]
		private GameObject tabButtonPrefab;
		[SerializeField]
		private GameObject pagePrefab;

		private TabButton firstTabButton = null;

		private void Awake()
		{
			for (int i = 0; i < setttingsObjects.Count; i++)
			{
				var currentObject = setttingsObjects[i];

				var tabButton = Instantiate(tabButtonPrefab, tabButtonsHolder).GetComponent<TabButton>();
				string tabLabel = "";
				var dnAttribute = currentObject.GetType().GetCustomAttributes(
					typeof(LocalizeNameAttribute), true).FirstOrDefault() as LocalizeNameAttribute;
				if (dnAttribute != null)
				{
					tabLabel = dnAttribute.nameAlias;
				}
				else
				{
					tabLabel = currentObject.GetType().Name;
				}
				tabButton.SetButtonText(tabLabel);
				if (firstTabButton == null)
				{
					firstTabButton = tabButton;
				}
				var page = Instantiate(pagePrefab, pagesHolder);
				tabGroup.AddTab(new TabPair(tabButton, page));

				var panelGenerator = page.GetComponent<PanelGenerator>();
				panelGenerator.GeneratePanelFor(currentObject);
			}
		}

		private void Start()
		{
			if (firstTabButton != null)
			{
				tabGroup.OnTabSelected(firstTabButton);
			}
		}

		public void OpenFolderWithSettings()
		{
			Debug.Log(Application.persistentDataPath);
			System.Diagnostics.Process.Start("explorer.exe", $"\"{Application.persistentDataPath.Replace("/", "\\")}\"");
		}
	}

}