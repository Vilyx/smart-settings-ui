﻿namespace SmartSettingsUI
{
	public interface ISaveable
	{
		void Save(string filename = "");
		void Load(string filename = "");
	}
}