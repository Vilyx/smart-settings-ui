﻿using System;
using System.Reflection;

namespace SmartSettingsUI
{
	public interface IFieldRepresentation
	{
		Type SupportedType { get; }
		Type[] RequiredAttributes { get; }
		void SetField(FieldInfo fieldInfo, object objectToConnect);
	}
}