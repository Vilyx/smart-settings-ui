using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;
using UnityEngine.UI;

namespace SmartSettingsUI
{
	public class FieldRepresentation : MonoBehaviour, IFieldRepresentation
	{
		public Text fieldLabel;
		public InputField fieldInputField;

		private object connectedObject;
		private FieldInfo connectedFieldInfo;

		public Type SupportedType { get; } = typeof(string);
		public Type[] RequiredAttributes { get; } = { };

		private void OnEnable()
		{
			fieldInputField.onEndEdit.AddListener(OnEndEdit);
		}

		private void OnDisable()
		{
			fieldInputField.onEndEdit.RemoveListener(OnEndEdit);
		}

		private void OnEndEdit(string newValue)
		{
			connectedFieldInfo.SetValue(connectedObject, newValue);
		}

		public void SetField(FieldInfo fieldInfo, object objectToConnect)
		{
			connectedFieldInfo = fieldInfo;
			connectedObject = objectToConnect;
			string fieldLabel = fieldInfo.Name;
			object[] attrs = fieldInfo.GetCustomAttributes(true);
			var attribute = attrs.Where(attr => attr is LocalizeNameAttribute).FirstOrDefault();
			if (attribute != null)
			{
				fieldLabel = (attribute as LocalizeNameAttribute).nameAlias;
			}
			SetFieldName(fieldLabel);
			SetFieldValue((string)fieldInfo.GetValue(objectToConnect));
		}

		public void SetFieldName(string fieldName)
		{
			fieldLabel.text = fieldName;
		}

		public void SetFieldValue(string fieldValue)
		{
			if (isActiveAndEnabled)
			{
				fieldInputField.onEndEdit.RemoveListener(OnEndEdit);
			}
			fieldInputField.text = fieldValue;
			if (isActiveAndEnabled)
			{
				fieldInputField.onEndEdit.AddListener(OnEndEdit);
			}
		}
	}
}