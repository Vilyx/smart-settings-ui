﻿using System;
using System.Linq;
using System.Reflection;
using UnityEngine;
using UnityEngine.UI;

namespace SmartSettingsUI
{
	public class IntInRangeFieldRepresentation : MonoBehaviour, IFieldRepresentation
	{
		public Text fieldLabel;
		public Slider slider;
		public InputField fieldInputField;

		private object connectedObject;
		private FieldInfo connectedFieldInfo;

		public Type SupportedType { get; } = typeof(int);
		public Type[] RequiredAttributes { get; } = { typeof(RangeAttribute) };

		private void OnEnable()
		{
			fieldInputField.onEndEdit.AddListener(OnEndEdit);
		}
		private void OnDisable()
		{
			fieldInputField.onEndEdit.RemoveListener(OnEndEdit);
		}

		private void OnEndEdit(string stringValue)
		{
			int newValue;
			if (int.TryParse(stringValue, out newValue))
			{
				connectedFieldInfo.SetValue(connectedObject, newValue);
				SetFieldValue(newValue);
			}
		}

		public void SetField(FieldInfo fieldInfo, object objectToConnect)
		{
			connectedFieldInfo = fieldInfo;
			connectedObject = objectToConnect;
			string fieldLabel = fieldInfo.Name;
			object[] attrs = fieldInfo.GetCustomAttributes(true);
			var localizeAttribute = attrs.Where(attr => attr is LocalizeNameAttribute).FirstOrDefault();
			if (localizeAttribute != null)
			{
				fieldLabel = (localizeAttribute as LocalizeNameAttribute).nameAlias;
			}
			var rangeAttribute = attrs.Where(attr => attr is RangeAttribute).FirstOrDefault();
			if (rangeAttribute != null)
			{
				slider.minValue = (rangeAttribute as RangeAttribute).min;
				slider.maxValue = (rangeAttribute as RangeAttribute).max;
			}
			SetFieldName(fieldLabel);
			SetFieldValue((int)fieldInfo.GetValue(objectToConnect));
		}

		public void SetFieldName(string fieldName)
		{
			fieldLabel.text = fieldName;
		}

		public void SetFieldValue(int fieldValue)
		{
			if (isActiveAndEnabled)
			{
				fieldInputField.onEndEdit.RemoveListener(OnEndEdit);
				slider.onValueChanged.RemoveListener(OnSliderChangedValue);
			}
			fieldInputField.text = fieldValue.ToString();
			slider.value = fieldValue;
			if (isActiveAndEnabled)
			{
				fieldInputField.onEndEdit.AddListener(OnEndEdit);
				slider.onValueChanged.AddListener(OnSliderChangedValue);
			}
		}

		private void OnSliderChangedValue(float newValue)
		{
			connectedFieldInfo.SetValue(connectedObject, (int)newValue);
			SetFieldValue((int)newValue);
		}
	}
}