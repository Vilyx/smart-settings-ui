﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;
using UnityEngine.UI;

namespace SmartSettingsUI
{
	public class IntFieldRepresentation : MonoBehaviour, IFieldRepresentation
	{
		public Text fieldLabel;
		public InputField fieldInputField;

		private object connectedObject;
		private FieldInfo connectedFieldInfo;

		public Type SupportedType { get; } = typeof(int);
		public Type[] RequiredAttributes { get; } = { };

		private void OnEnable()
		{
			fieldInputField.onEndEdit.AddListener(OnEndEdit);
		}
		private void OnDisable()
		{
			fieldInputField.onEndEdit.RemoveListener(OnEndEdit);
		}

		private void OnEndEdit(string stringValue)
		{
			int newValue;
			if (int.TryParse(stringValue, out newValue))
			{
				connectedFieldInfo.SetValue(connectedObject, newValue);
			}
		}

		public void SetField(FieldInfo fieldInfo, object objectToConnect)
		{
			connectedFieldInfo = fieldInfo;
			connectedObject = objectToConnect;
			string fieldLabel = fieldInfo.Name;
			object[] attrs = fieldInfo.GetCustomAttributes(true);
			var localizeAttribute = attrs.Where(attr => attr is LocalizeNameAttribute).FirstOrDefault();
			if (localizeAttribute != null)
			{
				fieldLabel = (localizeAttribute as LocalizeNameAttribute).nameAlias;
			}
			SetFieldName(fieldLabel);
			SetFieldValue((int)fieldInfo.GetValue(objectToConnect));
		}

		public void SetFieldName(string fieldName)
		{
			fieldLabel.text = fieldName;
		}

		public void SetFieldValue(int fieldValue)
		{
			if (isActiveAndEnabled)
			{
				fieldInputField.onEndEdit.RemoveListener(OnEndEdit);
			}
			fieldInputField.text = fieldValue.ToString();
			if (isActiveAndEnabled)
			{
				fieldInputField.onEndEdit.AddListener(OnEndEdit);
			}
		}
	}
}