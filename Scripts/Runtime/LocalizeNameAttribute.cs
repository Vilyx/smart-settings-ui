﻿namespace SmartSettingsUI
{
	[System.AttributeUsage(System.AttributeTargets.Field | System.AttributeTargets.Class | System.AttributeTargets.Struct)]
	public class LocalizeNameAttribute : System.Attribute
	{
		public string nameAlias;

		public LocalizeNameAttribute(string nameAlias)
		{
			this.nameAlias = nameAlias;
		}
	}
}