using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace SmartSettingsUI
{
	public class PanelGenerator : MonoBehaviour
	{
		public List<GameObject> fieldsRepresentations;
		public Transform panelRoot;
		[SerializeField]
		private GameObject saveableButtonsPrefab;
		[SerializeField]
		private GameObject separatorPrefab;
		[SerializeField]
		private GameObject spacerPrefab;

		private object targetObject;
		private Dictionary<Type, List<GameObject>> representationsDictionary = new Dictionary<Type, List<GameObject>>();

		private void Awake()
		{
			InitRepresentations();
		}

		public void GeneratePanelFor(object targetObject)
		{
			this.targetObject = targetObject;
			GenerateFields();
			AddSpacer();
			if (targetObject is ISaveable)
			{
				AddSaveableButtons((ISaveable)targetObject);
			}
		}

		private void InitRepresentations()
		{
			fieldsRepresentations = fieldsRepresentations.OrderBy(x => -x.GetComponent<IFieldRepresentation>().RequiredAttributes.Length).ToList();
			foreach (var fieldRepresentation in fieldsRepresentations)
			{
				var supportedFieldType = fieldRepresentation.GetComponent<IFieldRepresentation>().SupportedType;
				if (!representationsDictionary.ContainsKey(supportedFieldType))
				{
					representationsDictionary[supportedFieldType] = new List<GameObject>();
				}
				representationsDictionary[supportedFieldType].Add(fieldRepresentation);
			}
		}

		private void GenerateFields()
		{
			Type type = targetObject.GetType();

			foreach (var fieldInfo in type.GetFields().Where(f => f.IsPublic))
			{
				string fieldLabel = fieldInfo.Name;
				Type fieldType = fieldInfo.FieldType;
				Debug.Log($"Name: {fieldLabel} Type: {fieldType}");
				object[] attrs = fieldInfo.GetCustomAttributes(true);
				GameObject prefab = GetFieldRepresentationByAttributes(representationsDictionary[fieldType], attrs);

				var fieldInstance = Instantiate(prefab, panelRoot);
				var fieldRepresentation = fieldInstance.GetComponent<IFieldRepresentation>();
				fieldRepresentation.SetField(fieldInfo, targetObject);
				AddSeparator();
			}
		}

		private void AddSeparator()
		{
			Instantiate(separatorPrefab, panelRoot);
		}

		private void AddSpacer()
		{
			Instantiate(spacerPrefab, panelRoot);
		}

		private void AddSaveableButtons(ISaveable targetObject)
		{
			var saveableController = Instantiate(saveableButtonsPrefab, panelRoot).GetComponent<SaveableButtonsController>();
			saveableController.Initialize(targetObject);
		}

		private GameObject GetFieldRepresentationByAttributes(List<GameObject> representations, object[] givenAttributes)
		{
			for (int i = 0; i < representations.Count; i++)
			{
				var representation = representations[i].GetComponent<IFieldRepresentation>();
				var requiredAttributes = representation.RequiredAttributes;
				bool foundProperRepresentation = true;
				for (int j = 0; j < requiredAttributes.Length; j++)
				{
					bool foundAttribute = false;
					for (int k = 0; k < givenAttributes.Length; k++)
					{
						if (givenAttributes[k].GetType() == requiredAttributes[j])
						{
							foundAttribute = true;
						}
					}
					if (!foundAttribute)
					{
						foundProperRepresentation = false;
						break;
					}
				}
				if (foundProperRepresentation)
				{
					return representations[i];
				}
			}
			return null;
		}
	}
}